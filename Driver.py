import numpy as np
import pygame, sys, time, math
from pygame.locals import *

SCREEN_DIMENSIONS = (1000, 1000)
WHITE = (255,255,255)
n=64
m=2 # set to max that calculateValueAtPoint can return

### Interesting patterns:
#
# Checkerboard : math.floor(10*math.sin(x/10)+ 10*math.cos(y/10)) % n 
# 3D thing     : (x % (1 + y) + y % (1 + x)) % n
# linear       : (x+y)% or (x-y)%n
# circular     : (x**2+y**2)%n
# hyperbolic   : (x**2-y**2)%n
# parabolic    : (x**2+y)%n
# sinusoidal   : math.floor(10*math.sin(x/10)+ y) % n
# pattern      : abs(math.ceil(math.sin(x/20) + math.cos(y/20)))
###########################################

def calculateValueAtPoint(x,y):
    return abs(math.ceil(math.sin(x/20) + math.cos(y/20)))
        

def getPic(width,height):
    l = [];
    
    for x in range(0, width):
        for y in range (0, height):
            c = int(calculateValueAtPoint(x, y) / m * 255) 
            if c > 255 or c < 0:
                print(c)
            l.append([[x,y],(c,c,c)])

    return l

# Sets up pyGame
surface = pygame.display.set_mode(SCREEN_DIMENSIONS, 0, 32)
pygame.display.set_caption('MATH')
pygame.init()
pygame.font.init()
font = pygame.font.SysFont("monospace", 35)


    
# Handles all rendering
def render():
    pygame.draw.rect(surface, WHITE, (0, 0, SCREEN_DIMENSIONS[0], SCREEN_DIMENSIONS[1]))

    pic = getPic(SCREEN_DIMENSIONS[0],SCREEN_DIMENSIONS[1])
    for i in range(0,SCREEN_DIMENSIONS[0]*SCREEN_DIMENSIONS[1]):
        pygame.draw.line(surface, pic[i][1], pic[i][0], pic[i][0])
    pygame.display.update()


render()
pygame.image.save(surface, "img.png")
